# Identificao de cachorros e suas respectivas raças em imagens
#Desenvolvedor: Holden Offmenn
#Data do Projeto: 08/01/2019


import argparse
from os import listdir

from classifier import classifier


def main():
    # Pega os argumentos de entrada a serem utilizados
    in_args = get_input_args()
    
    #Retorna dicionário com chace valor a partir dos arquivos da pasta
    answers_dic = get_pet_labels(in_args.dir)
    
    result_dic = classify_images(in_args.dir, answers_dic, in_args.arch)
    
    adjust_results_for_isadog(result_dic, in_args.dogfile)
    
    print("\n     MATCH:")
    n_match = 0
    n_nomatch = 0
    
    for key in result_dic:
        if result_dic[key][2] == 1:
            n_match += 1
            print ("Real: %-26s      Classifier: %-30s   PetLablDog:  %1d              ClassLabelDog: %1d" % (result_dic[key][0],
                                                                                                    result_dic[key][1],
                                                                                                    result_dic[key][2],
                                                                                                    result_dic[key][3]))
    
    print("\n\n      NO MATCH:")
    for key in result_dic:
        if result_dic[key][2] == 0:
            n_nomatch += 1
            print ("Real: %-26s      Classifier: %-30s   PetLablDog:  %1d               ClassLabelDog: %1d" % (result_dic[key][0],
                                                                                                    result_dic[key][1],
                                                                                                    result_dic[key][2],
                                                                                                    result_dic[key][3]))

    print("\n\n #Total Images", n_match + n_nomatch, "#Matchs:", n_match, "#Not Matchs: ", n_nomatch)
    
def get_input_args():
    #Cria um parse entrada e referencia os argumentos a serem utilizados
    parse = argparse.ArgumentParser()
    
    # Argumento do caminho onde as imagens estão
    parse.add_argument('--dir', type=str, default='pet_images/' ,
                       help='path to folder of images')
    
    # Argumento de modelo de treinamento a ser utilizado na RNA, no caso o VGG
    parse.add_argument('--arch', type=str, default='vgg',
                       help='chosen model')
    
    # Argumento de entrada do nome do arquivo com os nomes das raças
    parse.add_argument('--dogfile', type=str, default='dognames.txt',
                       help='text file that has dognames')

    # Retorna um arquivo ArgParse com as 3 informações
    return parse.parse_args()

#Dicionário de labels para armezar os dados dos arquivos
def get_pet_labels(image_dir):
    #Entrada dos arquivos
    in_files = listdir(image_dir)
    
    #Cria o dicionário vazio
    petlabels_dic = dict()
    
    #Percorre e processa todos os arquivos da pasta
    for i in range(0, len(in_files), 1):
        #Verifica apenas arquivos que não começam com (.)
        if in_files[i][0] != ".":
            # Divide o nome em uma lista, baseado no (_)
            image_name = in_files[i].split("_")
            #Cria um label string
            pet_label = ""
            
            #Verifica as divisoes dos nomes dentro da lista image_name
            for word in image_name:
                #Se só contém letras
                if word.isalpha():
                    #Adiciona no pet_label com um espaço no final
                    pet_label += word.lower() + " "
            #Remove os espaços em branco
            pet_label = pet_label.strip()

            
            #Se o nome não existe no dicionário, ele é adicionado
            if in_files[i] not in petlabels_dic:
                # Adiciona uma entrada no dicionário pegando a chave que
                # é o nome do arquivo e atribuindo o valor(label) que é o nome
                # do 'cachorro' tratado
                petlabels_dic[in_files[i]] = pet_label                
            else:
                print("Atenção: Arquivo duplicado na pasta! -> ", in_files[i])
                

    return petlabels_dic


def classify_images(images_dir, petlabel_dic, model):
    
    #Cria dicionário vazio
    results_dic = dict()
    
    #Processa todos os arquivos em petlabel_dic
    for key in petlabel_dic:
        
        # Classifica as imagens onde a entrada é o caminho do arquivo (Caminho + Nome do Arquivo) mais
        # o modelo de CNN a ser trabalhado e a saída é o valor do modelo
        # O classifier classifica os nomes baseado em uma lista com raças de animais
        model_label = classifier(images_dir+key, model)
        
        #Converte para minusculo e sem espaco
        model_label = model_label.lower()
        model_label = model_label.strip()
        
        # Atribui o valor(label) da chave para truth
        truth = petlabel_dic[key]
        
        #Verifica se a label pode ser encontrada dentro do retorno do classifier(model_label)
        found = model_label.find(truth)
        
        #Se for encontrado alguma coisa, verifica se os arquivos os arquivos são do mesmo tamanho
        # ou elas se equivalem de alguma forma
        if found >=0:
            
            if( (found == 0 and len(truth)==len(model_label)) or 
               ( ((found == 0) or (model_label[found - 1] == " ")) and
               ( (found + len(truth) == len(model_label)) or 
               (model_label[found + len(truth): found + len(truth) + 1] in
                (","," "))
                        )
                    )
                ):
                if key not in results_dic:
                    results_dic[key] = [truth, model_label, 1]
            # Não se equivalem        
            else:
                if key not in results_dic:
                    results_dic[key] = [truth, model_label, 0]
        # Não se equivalem
        else:
            if key not in results_dic:
                results_dic[key] = [truth, model_label, 0]
    
    return results_dic     

def adjust_results_for_isadog(results_dic, dogfile):
    """
    Ajusta os resultados do dicionário para determinar se o classificador classificou corretamente
    as imagens como cachorro ou como não cachorro, especialmente quando as labels não combinam.
    Demonstra se o modelo de arquitetura classifica corretamente as imagens de cachorro mesmo se
    pegar a raça errada
    """
    
    dognames_dic = dict()
    
    #Lê o arquivo de entrada
    with open(dogfile, "r") as infile:
        line = infile.readline()
        
        
        while line != "":
            
            line = line.rstrip()
            
            
            if line not in dognames_dic:
                dognames_dic[line] = 1
            else:
                print("** Atenção: Nome de cachorro duplicado", line)
                
            line = infile.readline()

    
    for key in results_dic:
        
        if results_dic[key][0] in dognames_dic:
            
            if results_dic[key][1] in dognames_dic:
                results_dic[key].extend((1, 1))
            
            else:
                results_dic[key].extend((1, 1))

        else:
            
            if results_dic[key][1] in dognames_dic:
                results_dic[key].extend((0, 1))
                
            else:
                results_dic[key].extend((0, 0)) 

if __name__ == "__main__":
    main()